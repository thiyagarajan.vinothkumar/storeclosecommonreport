﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Diagnostics;
using System.IO;

namespace NightlyRoutineMonitor
{
    public class EmailService
    {
        private bool isStoreClose ;
        private bool isStoreOpen ;

        private List<StoreOpenFailureDetails> storesOpenFailureDetails = null;
        private List<StoreCloseFailureDetails> storesCloseFailureDetails = null;

        public static Monitor.Output _output = null;
        
        public EmailService(bool isStoreOpen, bool isStoreClose, List<StoreOpenFailureDetails> storesOpenFailureDetails, List<StoreCloseFailureDetails> storesCloseFailureDetails)
        {
            this.isStoreClose = isStoreClose;
            this.isStoreOpen = isStoreOpen;
            this.storesOpenFailureDetails = storesOpenFailureDetails;
            this.storesCloseFailureDetails = storesCloseFailureDetails;
        }

        /// <summary>
        /// This method will send success email for store open/close with list of stores
        /// </summary>
        /// <param name="storeList"></param>
        public void SendSuccessMail(List<Store> storeList)
        {
            try
            {
                string mailToAddress = ConfigurationManager.AppSettings["SupportTeamMail"].ToString();
                string mailFromAddress = ConfigurationManager.AppSettings["FromMail"].ToString();
                MailMessage mail = new MailMessage();
                MailAddress from = new MailAddress(mailFromAddress);
                mail.From = from;
                mail.To.Add(mailToAddress);
                StringBuilder body = new StringBuilder();
                string filename = string.Empty;

                MemoryStream stream = new MemoryStream();
                
                StreamWriter writer = new StreamWriter(stream);

                if (isStoreOpen)
                {
                    mail.Subject = "Auto Store Open - Success Report for " + DateTime.Now.ToString("dd/MM/yyyy");

                    body.AppendLine("The attached text file has a list of stores where the auto store open has been successful <br/>");

                    writer.WriteLine("Auto Store Open - Success Report for " + DateTime.Now.ToString("dd/MM/yyyy"));
                    writer.WriteLine();
                    writer.WriteLine("The following stores have opened  successfully" + Environment.NewLine);

                    filename = "AutoStoreOpen-SuccessReportfor" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                }

                if (isStoreClose)
                {
                    mail.Subject = "Auto Store Close - Success Report for " + DateTime.Now.ToString("dd/MM/yyyy");

                    body.AppendLine("The attached text file has a list of stores where the auto store close has been successful <br/>");

                    writer.WriteLine("Auto Store Close - Success Report for " + DateTime.Now.ToString("dd/MM/yyyy"));
                    writer.WriteLine();
                    writer.WriteLine("The following stores have closed  successfully" + Environment.NewLine);

                    filename = "AutoStoreClose-SuccessReportfor" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                }

                body.AppendLine(Environment.NewLine);
                mail.Body = body.ToString();
                mail.IsBodyHtml = true;

                foreach (var item in storeList)
                    writer.WriteLine(item._storeId.ToString());

                writer.Flush();
                stream.Position = 0;

                mail.Attachments.Add(new Attachment(stream, filename, "text/plain"));

                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                smtp.Port = Convert.ToInt16(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                if (ConfigurationManager.AppSettings["Env"].ToString().ToUpper() == "DEV")
                {
                    smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
                    smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
                }
                smtp.Send(mail);
                mail.Dispose();
                _output.Write("SendSuccessMail Triggered mail to support team : " + mailToAddress + " at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                Trace.WriteLine("SendSuccessMail Triggered mail to support team : " + mailToAddress + " at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                
            }
            catch (Exception ex)
            {
                _output.Write("SendSuccessMail Exception occurred while triggering mail to support team : " + ex.Message.ToString() + " at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                Trace.WriteLine("SendSuccessMail Exception occurred while triggering mail to support team : " + ex.Message.ToString() + " at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                throw ex;
            }
        }

        /// <summary>
        /// This method will send failure email for store open/close with list of stores failure details
        /// </summary>
        /// <param name="storesFailureDetails"></param>
        public void SendFailureMail()
        {
            try
            {
                string mailToAddress = ConfigurationManager.AppSettings["SupportTeamMail"].ToString();
                string mailFromAddress = ConfigurationManager.AppSettings["FromMail"].ToString();
                MailMessage mail = new MailMessage();
                MailAddress from = new MailAddress(mailFromAddress);
                mail.From = from;
                mail.To.Add(mailToAddress);
                StringBuilder body = new StringBuilder();
                string fileName = string.Empty;

                MemoryStream stream = new MemoryStream();
                
                StreamWriter writer = new StreamWriter(stream);

                if (isStoreOpen)
                {
                    mail.Subject = "Auto Store Open - Failure Report for " + DateTime.Now.ToString("dd/MM/yyyy");

                    body.AppendLine("The attached text file has a list of stores which have problems with their Store Open or it is still in progress <br/>");

                    writer.WriteLine("Auto Store Open - Failure Report for " + DateTime.Now.ToString("dd/MM/yyyy"));
                    writer.WriteLine();
                    writer.WriteLine("The following stores have problems with their Store Open or it is still in progress" + Environment.NewLine);
                    writer.WriteLine(String.Format("{0,-12}{1,-10}{2,-14}{3,-14}", "Date", "Time", "Store Number", "Error Detail"));
                    foreach (var item in storesOpenFailureDetails)
                    {
                        writer.WriteLine(String.Format("{0,-12}{1,-10}{2,-14}{3,-14}", item.Date, item.Time, item.StoreNumber ,item.ErrorDetail));
                    }

                    fileName = "AutoStoreOpen-FailureReportfor" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                }
                if (isStoreClose)
                {
                    mail.Subject = "Auto Store Close - Failure Report for " + DateTime.Now.ToString("dd/MM/yyyy");

                    body.AppendLine("The attached text file has a list of stores which have problems with their Store Closure or it is still in progress <br/>");

                    writer.WriteLine("Auto Store Close - Failure Report for " + DateTime.Now.ToString("dd/MM/yyyy"));
                    writer.WriteLine();
                    writer.WriteLine("The following stores have problems with their Store Closure or it is still in progress" + Environment.NewLine);
                    writer.WriteLine(String.Format("{0,-12}{1,-10}{2,-14}{3,-35}{4,-20}", "Date", "Time", "Store Number", "Last Successful Executed Task", "Error Detail"));
                    string lastSuccessfulExecutedTask = "";
                    foreach (var item in storesCloseFailureDetails)
                    {
                        if (item.LastSuccessfulExecutedTask.Length > 30)
                            lastSuccessfulExecutedTask = item.LastSuccessfulExecutedTask.ToString().Substring(0, 30);
                        else
                            lastSuccessfulExecutedTask = item.LastSuccessfulExecutedTask.ToString();

                        writer.WriteLine(String.Format("{0,-12}{1,-10}{2,-14}{3,-35}{4,-20}", item.Date, item.Time, item.StoreNumber, lastSuccessfulExecutedTask, item.ErrorDetail));
                    }

                    fileName = "AutoStoreClose-FailureReportfor" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                }

                body.AppendLine(Environment.NewLine);
                mail.Body = body.ToString();
                mail.IsBodyHtml = true;

                writer.Flush();
                stream.Position = 0;

                mail.Attachments.Add(new Attachment(stream, fileName, "text/plain"));
               
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                smtp.Port = Convert.ToInt16(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                if (ConfigurationManager.AppSettings["Env"].ToString().ToUpper() == "DEV")
                {
                    smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
                    smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
                }
                smtp.Send(mail);
                mail.Dispose();
                _output.Write("SendFailureMail Triggered mail to support team : " + mailToAddress + " at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                Trace.WriteLine("SendFailureMail Triggered mail to support team : " + mailToAddress + " at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                
            }
            catch (Exception ex)
            {
                _output.Write("SendFailureMail Exception occurred while triggering mail to support team : " + ex.Message.ToString() + " at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                Trace.WriteLine("SendFailureMail Exception occurred while triggering mail to support team : " + ex.Message.ToString() + " at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                throw ex;
            }
        }
    }
}
