﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace NightlyRoutineMonitor
{
    public class Monitor
    {
        private bool isStoreClose = false;
        private bool isStoreOpen = false;

        private string storeOpenStartTime  = null;
        private string storeOpenEndTime    = null;
        private string storeCloseStartTime = null;
        private string storeCloseEndTime   = null;

        List<Store> storeSuccessList = new List<Store>();
        List<StoreOpenFailureDetails> storesOpenFailureDetailsList = null;
        List<StoreCloseFailureDetails> storesCloseFailureDetailsList = null;
        private Output _output = null;

        public void Run()
        {
            _output = new Output();
            EmailService._output = this._output;
            try
            {
                _output.Write("Vaildation for all Stores  Started at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                Trace.WriteLine("Vaildation for all Stores  Started at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            
                isStoreOpen = CheckEmailTimeIsValid(storeOpenStartTime, storeOpenEndTime);
                isStoreClose = CheckEmailTimeIsValid(storeCloseStartTime, storeCloseEndTime);

                if (isStoreOpen)
                {
                    _output.Write("Is Store Open");
                    Trace.WriteLine("Is Store Open");
                }
                else if (isStoreClose)
                {
                    _output.Write("Is Store Close");
                    Trace.WriteLine("Is Store Close");
                }
                else
                {
                    return;
                }
                storesCloseFailureDetailsList = new List<StoreCloseFailureDetails>();
                storesOpenFailureDetailsList = new List<StoreOpenFailureDetails>();

                List<Store> lstStore = GetStoresList();

                if (lstStore.Count > 0)
                {
                    foreach (Store store in lstStore.AsEnumerable())
                    {
                        try
                        {
                            _output.Write("Store Number " + store._storeId+ " Started Checking Details");
                            Trace.WriteLine("Store Number " + store._storeId + " Started Checking Details");
                            if (!IsStoreInAutoMode(store._storeId, "OASYS"))
                            {
                                continue;
                            }

                            if (isStoreClose)
                            {
                                bool isStoreCloseStarted = IsStoreCloseStarted(store._storeId, "OASYS");
                                bool isStoreCompleted = IsStoreCloseCompleted(store._storeId, "OASYS");
                                DataRow lastTask = GetLastCompletedTask(store._storeId, "OASYS");

                                StoreCloseFailureDetails obj = new StoreCloseFailureDetails();

                                if (isStoreCloseStarted && isStoreCompleted)
                                {
                                    //Store Close completed
                                    _output.Write("Validation Status - Store Close completed ");
                                    Trace.WriteLine("Validation Status - Store Close completed ");
                                    storeSuccessList.Add(store);
                                }

                                if (isStoreCloseStarted == true && isStoreCompleted == false)
                                {
                                    //Store Close in progress or Failed
                                    _output.Write("Validation Status - Store Close in progress / Failed ");
                                    Trace.WriteLine("Validation Status - Store Close in progress / Failed ");
                                    obj.Date = DateTime.Now.ToString("dd/MM/yyyy");
                                    obj.Time = DateTime.Now.ToString("HH:mm:ss");
                                    obj.StoreNumber = store._storeId;
                                    if (lastTask != null)
                                        obj.LastSuccessfulExecutedTask = Convert.ToString(lastTask["TASK"]) + " " + Convert.ToString(lastTask["DESCR"]).Trim();
                                    else
                                        obj.LastSuccessfulExecutedTask = "";
                                    obj.ErrorDetail = "Failed";
                                    storesCloseFailureDetailsList.Add(obj);
                                }
                                else if (isStoreCloseStarted == false)
                                {
                                    //Store Close not started
                                    _output.Write("Validation Status - Store Close not started ");
                                    Trace.WriteLine("Validation Status - Store Close not started ");
                                    obj.Date = DateTime.Now.ToString("dd/MM/yyyy");
                                    obj.Time = DateTime.Now.ToString("HH:mm:ss");
                                    obj.StoreNumber = store._storeId;
                                    obj.LastSuccessfulExecutedTask = "";
                                    obj.ErrorDetail = "Not Started";
                                    storesCloseFailureDetailsList.Add(obj);
                                }
                            }

                            if (isStoreOpen)
                            {
                                bool isStoreOpenCompleted = IsStoreOpenCompleted(store._storeId, "OASYS");
                                StoreOpenFailureDetails obj = new StoreOpenFailureDetails();

                                if (isStoreOpenCompleted)
                                {
                                    //Store Open completed
                                    _output.Write("Validation Status - Store Open completed ");
                                    Trace.WriteLine("Validation Status - Store Open completed ");
                                    storeSuccessList.Add(store);
                                }
                                else
                                {
                                    //Store Open failed
                                    _output.Write("Validation Status - Store Open failed ");
                                    Trace.WriteLine("Validation Status - Store Open failed ");
                                    obj.Date = DateTime.Now.ToString("dd/MM/yyyy");
                                    obj.Time = DateTime.Now.ToString("HH:mm:ss");
                                    obj.StoreNumber = store._storeId;
                                    obj.ErrorDetail = "Failed";
                                    storesOpenFailureDetailsList.Add(obj);
                                }
                            }
                            
                        }
                        catch (Exception ex)
                        {
                            _output.Write("Store Number " + store._storeId +" Failed Message - "+ex.Message );
                            Trace.WriteLine("Store Number " + store._storeId + " Failed Message - " + ex.Message);
                            if (isStoreOpen)
                            {
                                StoreOpenFailureDetails obj = new StoreOpenFailureDetails();
                                obj.Date = DateTime.Now.ToString("dd/MM/yyyy");
                                obj.Time = DateTime.Now.ToString("HH:mm:ss");
                                obj.StoreNumber = store._storeId;
                                obj.ErrorDetail = ex.Message;
                                storesOpenFailureDetailsList.Add(obj);
                                
                            }
                            if (isStoreClose)
                            {
                                StoreCloseFailureDetails obj = new StoreCloseFailureDetails();
                                obj.Date = DateTime.Now.ToString("dd/MM/yyyy");
                                obj.Time = DateTime.Now.ToString("HH:mm:ss");
                                obj.StoreNumber = store._storeId;
                                obj.LastSuccessfulExecutedTask = "";
                                obj.ErrorDetail = ex.Message;
                                storesCloseFailureDetailsList.Add(obj);
                                
                            }
                        }
                        _output.Write("Store Number " + store._storeId + " Completed Validation ");
                        Trace.WriteLine("Store Number " + store._storeId + " Completed Validation ");
                    }

                    EmailService emailService = new EmailService(isStoreOpen, isStoreClose, storesOpenFailureDetailsList, storesCloseFailureDetailsList);
                    if (storeSuccessList!=null && storeSuccessList.Count > 0)
                        emailService.SendSuccessMail(storeSuccessList);
                    if ((storesCloseFailureDetailsList != null && storesCloseFailureDetailsList.Count > 0) || (storesOpenFailureDetailsList != null && storesOpenFailureDetailsList.Count > 0))
                        emailService.SendFailureMail();
                }
                _output.Write("Vaildation for all Stores Completed at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                Trace.WriteLine("Vaildation for all Stores Completed at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            }
            catch (Exception ex)
            {
                _output.Write("NightlyRoutineMonitor Failed " + ex.Message + " at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                Trace.WriteLine("NightlyRoutineMonitor Failed " + ex.Message + " at " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            }
            _output.Dispose();
        }

        public Monitor()
        {
            storeOpenStartTime = ConfigurationManager.AppSettings["StoreOpenStartTime"].ToString();
            storeOpenEndTime = ConfigurationManager.AppSettings["StoreOpenEndTime"].ToString();
            storeCloseStartTime = ConfigurationManager.AppSettings["StoreCloseStartTime"].ToString();
            storeCloseEndTime = ConfigurationManager.AppSettings["StoreCloseEndTime"].ToString();
        }
               

        /// <summary>
        /// This method will get the connection string
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="dataBase"></param>
        private string GetStoreConnectionString(string dataSource, string dataBase)
        {
            try
            {
                SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();
                connectionString.DataSource = @dataSource;
                connectionString.InitialCatalog = dataBase;
                connectionString.IntegratedSecurity = true;
                connectionString.Pooling = true;
                return connectionString.ConnectionString;
            }
            catch (Exception ex)
            {
                _output.Write("GetStoreConnectionString Failed " + ex.Message);
                Trace.WriteLine("GetStoreConnectionString Failed " + ex.Message);
                throw ex;
            } 
        }

        /// <summary>
        /// This method will check if the current time is between the strStart and strEnd
        /// </summary>
        /// <param name="strStart"></param>
        /// <param name="strEnd"></param>
        private bool CheckEmailTimeIsValid(String strStart, String strEnd)
        {
            try
            {
                var startTime = TimeSpan.Parse(strStart);
                var startDate = DateTime.Today.Add(startTime);

                var EndTime = TimeSpan.Parse(strEnd);
                var EndDate = DateTime.Today.Add(EndTime);

                if (DateTime.Now >= startDate && DateTime.Now <= EndDate)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                _output.Write("CheckEmailTimeIsValid Failed " + ex.Message);
                Trace.WriteLine("CheckEmailTimeIsValid Failed " + ex.Message);
                throw ex;
            }
            
        }

        /// <summary>
        /// This method will get the store list 
        /// </summary>
        private List<Store> GetStoresList()
        {
            try
            {
                DataSet storeList = FetchStoreList();

                List<Store> lstStores = new List<Store>();
                if (storeList != null && storeList.Tables[0].Rows.Count != 0)
                {

                    lstStores = (from rw in storeList.Tables[0].AsEnumerable()
                                 select new Store()
                                 {
                                     _storeId = Convert.ToString((rw["Store"]))
                                 
                                 }).ToList();
                }
                foreach (Store item in lstStores)
                {
                    _output.Write(item._storeId);
                    Trace.WriteLine(item._storeId);
                }
                return lstStores;
            }
            catch(Exception ex) 
            {
                _output.Write("GetStoresList Failed " + ex.Message);
                Trace.WriteLine("GetStoresList Failed " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// This method will check if Stores list is present or not
        /// </summary>
        private DataSet FetchStoreList()
        {
            try
            {
                string maxId = ConfigurationManager.AppSettings["StoreMax"].ToString();
                string dataSource = ConfigurationManager.AppSettings["StoreServer"].ToString();
                string dataBase = ConfigurationManager.AppSettings["StoreDataBase"].ToString();
                string Query = "select 'SRV' + convert(varchar,Id) As Store from Stores where [Status] in ('L','P','T','W') AND IsClosed={0} AND Id<{1}";
            
                Query = string.Format(Query, 0, maxId);
                DataSet dsstoreList= GetData(GetStoreConnectionString(dataSource, dataBase), Query);

                _output.Write("FetchStoreList Fetched " + dsstoreList.Tables[0].Rows.Count + " Stores");
                Trace.WriteLine("FetchStoreList Fetched " + dsstoreList.Tables[0].Rows.Count + " Stores"); 

                return dsstoreList;
            }
            catch (Exception ex)
            {
                _output.Write("FetchStoreList Failed " + ex.Message);
                Trace.WriteLine("FetchStoreList Failed " + ex.Message);
                throw ex;
            }
            
        }

        /// <summary>
        /// This method will check if Store open is completed or not
        /// </summary>
        private bool IsStoreOpenCompleted(string dataSource, string dataBase)
        {
            //1.checking master workstation is active
            try
            {
                string Query = "select ISNULL(IsActive,0) As IsActive from workstationconfig where id=(select ISNULL(MAST,0) from sysopt)";
                DataSet dsNitlogs = GetData(GetStoreConnectionString(dataSource, dataBase), Query);
                if (dsNitlogs == null) return false;
                if (dsNitlogs.Tables[0].Rows.Count == 0) return false;
                if (Convert.ToBoolean(dsNitlogs.Tables[0].Rows[0]["IsActive"]) == false)
                {
                    return false;
                }

                //2.checking Nitmascs status
                Query = "select status from nitmasconfig";
                dsNitlogs = GetData(GetStoreConnectionString(dataSource, dataBase), Query);
                if (dsNitlogs == null) return false;
                if (dsNitlogs.Tables[0].Rows.Count == 0) return false;
                string strStatus = Convert.ToString(dsNitlogs.Tables[0].Rows[0]["status"]);
                if (String.IsNullOrEmpty(strStatus))
                    return false;

                if (strStatus.Trim().ToLower().CompareTo("yet to start") == 0)
                    return true;

                return false;
            }
            catch (Exception ex)
            {
                _output.Write("IsStoreOpenCompleted Failed " + ex.Message);
                Trace.WriteLine("IsStoreOpenCompleted Failed " + ex.Message);
                throw ex;
            }
            
        }

        /// <summary>
        /// This method will check if Storeis Auto Mode or not
        /// </summary>
        private bool IsStoreInAutoMode(string dataSource, string dataBase)
        {
            try
            {
                string Query = "select Toggle from nitmasconfig";
                DataSet dsNitlogs = GetData(GetStoreConnectionString(dataSource, dataBase), Query);
                if (dsNitlogs == null) return false;
                if (dsNitlogs.Tables[0].Rows.Count == 0) return false;
                return Convert.ToBoolean(dsNitlogs.Tables[0].Rows[0]["Toggle"]);
                
            }
            catch (Exception ex)
            {
                _output.Write("IsStoreInAutoMode Failed " + ex.Message);
                Trace.WriteLine("IsStoreInAutoMode Failed " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// This method will check if Store Close is started or not
        /// </summary>
        private bool IsStoreCloseStarted(string dataSource, string dataBase)
        {
            DateTime date = DateTime.Today;
            date= date.AddDays(-1);
            string strDate=date.ToString("yyyy-MM-dd");
            
            string Query = "SELECT isnull(Convert(Varchar(500),SDAT,106),'') as SDAT, isnull(Convert(Varchar(500),EDAT,106),'') as EDAT FROM NITLOG WHERE  DATE1 = '" + strDate + "'  And task = '005' order by TASK desc ";
            try
            {
                DataSet dsNitlogs = GetData(GetStoreConnectionString(dataSource, dataBase), Query);
                if (dsNitlogs == null) return false;
                if (dsNitlogs.Tables[0].Rows.Count == 0) return false;
                if (String.IsNullOrEmpty(Convert.ToString(dsNitlogs.Tables[0].Rows[0]["SDAT"])) || String.IsNullOrEmpty(Convert.ToString(dsNitlogs.Tables[0].Rows[0]["EDAT"])))
                {
                    return false;
                }
                return true;
            }
            catch(Exception ex) 
            {
                _output.Write("IsStoreCloseStarted Failed " + ex.Message);
                Trace.WriteLine("IsStoreCloseStarted Failed " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// This method will check if Store Close is completed or not
        /// </summary>
        private bool IsStoreCloseCompleted(string dataSource, string dataBase)
        {
            DateTime date = DateTime.Today;
            date = date.AddDays(-1);
            string strDate = date.ToString("yyyy-MM-dd");

            string Query = "SELECT isnull(Convert(Varchar(500),SDAT,106),'') as SDAT, isnull(Convert(Varchar(500),EDAT,106),'') as EDAT FROM NITLOG WHERE  DATE1 = '" + strDate + "'  And task = '505' order by TASK desc ";
            //change for datasource and dataBase parameter
            try
            {
                DataSet dsNitlogs = GetData(GetStoreConnectionString(dataSource, dataBase), Query);
                if (dsNitlogs == null) return false;
                if (dsNitlogs.Tables[0].Rows.Count == 0) return false;
                if (String.IsNullOrEmpty(Convert.ToString(dsNitlogs.Tables[0].Rows[0]["SDAT"])) || String.IsNullOrEmpty(Convert.ToString(dsNitlogs.Tables[0].Rows[0]["EDAT"])))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                _output.Write("IsStoreCloseCompleted Failed " + ex.Message);
                Trace.WriteLine("IsStoreCloseCompleted Failed " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// This method will get the last completed task in store open / close
        /// </summary>
        private DataRow GetLastCompletedTask(string dataSource, string dataBase)
        {
            DateTime date = DateTime.Today;
            date = date.AddDays(-1);
            string strDate = date.ToString("yyyy-MM-dd");

            string Query = "SELECT TOP 1 TASK,DESCR FROM NITLOG WHERE  DATE1 = '" + strDate + "'  And SDAT is not null AND EDAT is not NULL order by TASK desc ";
             
            try
            {
                DataSet dsNitlogs = GetData(GetStoreConnectionString(dataSource, dataBase), Query);
                if (dsNitlogs == null) return null;
                if (dsNitlogs.Tables[0].Rows.Count == 0) return null;
                return dsNitlogs.Tables[0].Rows[0];
            }
            catch (Exception ex)
            {
                _output.Write("GetLastCompletedTask Failed " + ex.Message);
                Trace.WriteLine("GetLastCompletedTask Failed " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// This method will get the data for the respective query pass to it as parameter
        /// </summary>
        /// <param name="Connection"></param>
        /// <param name="query"></param>
        private DataSet GetData(string Connection,string query)
        {
            DataSet dsNitlogs = null;
            try
            {
                using (var con = new SqlConnection(Connection))
                {
                    if (con.State == ConnectionState.Closed)
                        con.Open();

                    using (var cmd = new SqlCommand(query, con))
                    {
                        using (var sda = new SqlDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (dsNitlogs = new DataSet())
                            {
                                sda.Fill(dsNitlogs, "Nitlog");
                                return dsNitlogs;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _output.Write("GetData Failed " + ex.Message);
                Trace.WriteLine("GetData Failed " + ex.Message);
                throw ex;
            }
            
        }

        /// <summary>
        /// This class will create the log file  
        /// </summary>
        public class Output : IDisposable
        {
            private StreamWriter _log = null;
            private StringBuilder _logString = new StringBuilder();

            public string header = "NightlyRoutineMonitor";

            //add log file generation based on transcation

            public Output()
            {
                SetLogPath();
            }

            private void DeleteExistingLog(String logpath)
            {
                string[] files = Directory.GetFiles(logpath, "NightlyRoutineMonitor_Log_*", SearchOption.AllDirectories);
                if (files.Count() > 0)
                {
                    foreach (string item in files)
                    {
                        try
                        {
                            // delete the files
                            File.Delete(item);
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                    }
                }
            }

            //add log file generation based on transcation
            private void SetLogPath()
            {
                string logpath;
                string strFileName;
                string strTime;
                const int intMaxFileLoops = 5;

                logpath = ConfigurationManager.AppSettings["FileLogPath"].ToString();
                //add log file generation based on transcation
                if (!Directory.Exists(logpath))
                    Directory.CreateDirectory(logpath);
                else
                    DeleteExistingLog(logpath);

                if (!logpath.EndsWith(@"\"))
                    logpath = logpath + @"\";

                for (int intIndex = 1; intIndex <= intMaxFileLoops; intIndex += 1)
                {
                    try
                    {
                        // log name - year, month, day, hour, minute, second, millisecond (padded left to 3 digits)
                        strTime = DateTime.Now.ToString("yyyyMMddHHmmss") + DateTime.Now.ToString("FFF").PadLeft(3, System.Convert.ToChar("0"));

                        strFileName = logpath + "NightlyRoutineMonitor_Log_" + strTime + ".txt";


                        if (!System.IO.File.Exists(strFileName))
                        {
                            _log = new StreamWriter(strFileName, true);

                            break;
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(System.Convert.ToInt32(Math.Floor(new Random().NextDouble() * 500)));
                        }
                    }
                    catch (IOException)//FileInUse
                    {
                        System.Threading.Thread.Sleep(System.Convert.ToInt32(Math.Floor(new Random().NextDouble() * 500)));
                        continue;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            public void Write()
            {
                if (_log != null)
                {
                    _logString.Append(Environment.NewLine);
                    //_log.WriteLine(Environment.NewLine);
                }
            }

            public void Write(string message)
            {
                StringBuilder sb = new StringBuilder();
                if (header != null)
                    sb.Append(header + ": ");
                if (message != null)
                    sb.Append(message.Trim());

                if (_log != null)
                {
                    _logString.Append(sb.ToString());
                    _log.WriteLine(sb.ToString());
                    Write();
                }
            }

            public string GetLogString()
            {
                return _logString.ToString();
            }


            private bool disposedValue = false;

            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposedValue)
                {
                    if (disposing)
                    {
                        if (_log != null)
                        {
                            _log.Flush();
                            _log.Close();
                            _log.Dispose();
                        }
                    }
                }
                this.disposedValue = true;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
    }
}
