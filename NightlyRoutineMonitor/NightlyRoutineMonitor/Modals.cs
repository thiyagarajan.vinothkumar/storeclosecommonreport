﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NightlyRoutineMonitor
{
    public class Store
    {
        public string _storeId { get; set; }
    }

    public class StoreOpenFailureDetails
    {
        public string Date { get; set; }
        public string Time { get; set; }
        public string StoreNumber { get; set; }
        public string ErrorDetail { get; set; }
    }

    public class StoreCloseFailureDetails
    {
        public string Date { get; set; }
        public string Time { get; set; }
        public string StoreNumber { get; set; }
        public string LastSuccessfulExecutedTask { get; set; }
        public string ErrorDetail { get; set; }
    }
}
