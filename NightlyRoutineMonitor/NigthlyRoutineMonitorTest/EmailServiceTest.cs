﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NightlyRoutineMonitor;
using NUnit.Framework;
namespace NigthlyRoutineMonitorTest
{
    [TestFixture]
    class EmailServiceTest
    {
        static List<Store> storeSuccessList = new List<Store>()
        {
            new Store{ _storeId = "SRV8803"},
            new Store{ _storeId = "SRV8804"},
            new Store{ _storeId = "SRV8805"},
            new Store{ _storeId = "SRV8806"},
            new Store{ _storeId = "SRV8807"}
        };
        static List<StoreOpenFailureDetails> storesOpenFailureDetailsList = new List<StoreOpenFailureDetails>()
        {
            new StoreOpenFailureDetails { Date = DateTime.Now.ToString("dd/MM/yyyy"), Time = DateTime.Now.ToString("HH:mm:ss") , StoreNumber = "SRV8234" , ErrorDetail = "Failed" },
            new StoreOpenFailureDetails { Date = DateTime.Now.ToString("dd/MM/yyyy"), Time = DateTime.Now.ToString("HH:mm:ss") , StoreNumber = "SRV8210" , ErrorDetail = "Failed" },
            new StoreOpenFailureDetails { Date = DateTime.Now.ToString("dd/MM/yyyy"), Time = DateTime.Now.ToString("HH:mm:ss") , StoreNumber = "SRV8214" , ErrorDetail = "Failed" },
            new StoreOpenFailureDetails { Date = DateTime.Now.ToString("dd/MM/yyyy"), Time = DateTime.Now.ToString("HH:mm:ss") , StoreNumber = "SRV8215" , ErrorDetail = "Failed" },
            new StoreOpenFailureDetails { Date = DateTime.Now.ToString("dd/MM/yyyy"), Time = DateTime.Now.ToString("HH:mm:ss") , StoreNumber = "SRV8216" , ErrorDetail = "Failed" },
        };
        static List<StoreCloseFailureDetails> storesCloseFailureDetailsList = new List<StoreCloseFailureDetails>()
        {
            new StoreCloseFailureDetails { Date = DateTime.Now.ToString("dd/MM/yyyy"), Time = DateTime.Now.ToString("HH:mm:ss") , StoreNumber = "SRV8234" ,LastSuccessfulExecutedTask = "005", ErrorDetail = "In Progress/Failed" },
            new StoreCloseFailureDetails { Date = DateTime.Now.ToString("dd/MM/yyyy"), Time = DateTime.Now.ToString("HH:mm:ss") , StoreNumber = "SRV8210" ,LastSuccessfulExecutedTask = "", ErrorDetail = "Not Started" },
            new StoreCloseFailureDetails { Date = DateTime.Now.ToString("dd/MM/yyyy"), Time = DateTime.Now.ToString("HH:mm:ss") , StoreNumber = "SRV8214" ,LastSuccessfulExecutedTask = "270", ErrorDetail = "In Progress/Failed" },
            new StoreCloseFailureDetails { Date = DateTime.Now.ToString("dd/MM/yyyy"), Time = DateTime.Now.ToString("HH:mm:ss") , StoreNumber = "SRV8215" ,LastSuccessfulExecutedTask = "", ErrorDetail = "Not Started" },
            new StoreCloseFailureDetails { Date = DateTime.Now.ToString("dd/MM/yyyy"), Time = DateTime.Now.ToString("HH:mm:ss") , StoreNumber = "SRV8216" ,LastSuccessfulExecutedTask = "235", ErrorDetail = "In Progress/Failed" },
        };

        EmailService storeOpenEmailService = new EmailService(true, false, storesOpenFailureDetailsList, storesCloseFailureDetailsList);
        EmailService storeCloseEmailService = new EmailService(false, true, storesOpenFailureDetailsList, storesCloseFailureDetailsList);

        //storeOpenSuccessMail
        [Test]
        public void storeOpenSendSuccessMail()
        {
            try 
            {
                storeOpenEmailService.SendSuccessMail(storeSuccessList);
                Assert.Pass();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            
        }
        //storeCloseFailureMail
        [Test]
        public void storeCloseSendSuccessMail()
        {
            try
            {
                storeCloseEmailService.SendSuccessMail(storeSuccessList);
                Assert.Pass();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            
        }
        //storeOpenFailureMail
        [Test]
        public void storeOpenSendFailureMail()
        {
            try
            {
                storeOpenEmailService.SendFailureMail();
                Assert.Pass();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            
        }
        //storeCloseFailureMail
        [Test]
        public void storeCloseSendFailureMail()
        {
            try
            {
                storeCloseEmailService.SendFailureMail();
                Assert.Pass();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            
        }
    }
}
