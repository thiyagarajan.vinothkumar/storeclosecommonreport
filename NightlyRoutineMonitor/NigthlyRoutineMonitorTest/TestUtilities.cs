﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
namespace NigthlyRoutineMonitorTest
{
    public class TestUtilities
    {
        public static object RunInstanceMethod(System.Type type, string strMethod, object objInstance, object[] objParams)
        {
            BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            return RunMethod(type, strMethod, objInstance, objParams, bindingAttr);
        }

        private static object RunMethod(System.Type type, string strMethod, object objInstance, object[] objParams, BindingFlags bindingAttr)
        {
            MethodInfo methodInfo;
            try
            {
                //Searches for the specified method, using the specified binding constraints.
                methodInfo = type.GetMethod(strMethod, bindingAttr);
                if (methodInfo == null)
                {
                    throw new ArgumentException("There is no method '" + strMethod + "' for type '" + type.ToString() + "'.");
                }
                //Invokes the method by the current instance using the specified parameters.
                object objRet = methodInfo.Invoke(objInstance, objParams);
                return objRet;
            }
            catch
            {
                throw;
            }
        }
    }
}
