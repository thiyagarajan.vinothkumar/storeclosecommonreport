﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NightlyRoutineMonitor;
using NUnit.Framework;
using System.Data;
using System.Data.SqlClient;

namespace NigthlyRoutineMonitorTest
{
    [TestFixture]
    class MonitorTest
    {
        string storeId = "WIN-FSOYSBSH5ZC";
        string dataBase = "Oasys";
        string connectionString = "Data Source=WIN-FSOYSBSH5ZC;Initial Catalog=Oasys;Integrated Security=True;Pooling=True";
        string strDate = DateTime.Today.AddDays(-2).ToString("yyyy-MM-dd");
        Monitor monitor = new Monitor();
        [Test]
        public void ExpectedConnectionString()
        {
            object obj = TestUtilities.RunInstanceMethod(typeof(Monitor), "GetStoreConnectionString", monitor, new object[2] { storeId , dataBase });
            string actualConnectionString = Convert.ToString(obj);
            Assert.AreEqual(connectionString, actualConnectionString);
        }

        [Test]
        public void ExpectedStoreOpenCheckEmailTimeIsValid()
        {
            bool expectedValue = false;
            string storeOpenStartTime = "07:00:00";
            string storeOpenEndTime = "07:59:59";
            if (DateTime.Now >= DateTime.Today.Add(TimeSpan.Parse(storeOpenStartTime)) && DateTime.Now <= DateTime.Today.Add(TimeSpan.Parse(storeOpenEndTime)))
                expectedValue =  true;
            object obj = TestUtilities.RunInstanceMethod(typeof(Monitor), "CheckEmailTimeIsValid", monitor, new object[2] { storeOpenStartTime, storeOpenEndTime });
            bool? actualValue = Convert.ToBoolean(obj);
            Assert.AreEqual(expectedValue,actualValue);
        }

        [Test]
        public void ExpectedStoreCloseCheckEmailTimeIsValid()
        {
            bool expectedValue = false;
            string storeCloseStartTime = "23:00:00";
            string storeCloseEndTime = "23:59:59";
            if (DateTime.Now >= DateTime.Today.Add(TimeSpan.Parse(storeCloseStartTime)) && DateTime.Now <= DateTime.Today.Add(TimeSpan.Parse(storeCloseEndTime)))
                expectedValue = true;
            object obj = TestUtilities.RunInstanceMethod(typeof(Monitor), "CheckEmailTimeIsValid", monitor, new object[2] { storeCloseStartTime, storeCloseEndTime });
            bool? actualValue = Convert.ToBoolean(obj);
            Assert.AreEqual(expectedValue, actualValue);
        }

        [Test]
        public void ExpectedFetchStoreList()
        {
            string query = "select 'SRV' + convert(varchar,Id) As Store from Stores where [Status] in ('L','P','T','W') AND IsClosed=0 AND Id<9000";
            DataSet actualData = (DataSet)TestUtilities.RunInstanceMethod(typeof(Monitor), "FetchStoreList", monitor, new object[0] {  });
            DataSet expectedData = GetData(connectionString, query);
            actualData.Merge(expectedData);
            bool foundChanges = actualData.HasChanges();
            Assert.IsFalse(foundChanges);
        }

        [Test]
        public void ExpectedIsStoreOpenCompleted()
        {
            bool expectedValue = true;
            string query = "select ISNULL(IsActive,0) As IsActive from workstationconfig where id=(select ISNULL(MAST,0) from sysopt)";
            DataSet dsNitlogs = GetData(connectionString, query);
            if (dsNitlogs == null || dsNitlogs.Tables[0].Rows.Count == 0 || Convert.ToBoolean(dsNitlogs.Tables[0].Rows[0]["IsActive"]) == false)
                expectedValue = false;
            query = "select status from nitmasconfig";
            dsNitlogs = GetData(connectionString, query);
            if (String.IsNullOrEmpty(Convert.ToString(dsNitlogs.Tables[0].Rows[0]["status"])))
                expectedValue = true;
            object obj = TestUtilities.RunInstanceMethod(typeof(Monitor), "IsStoreOpenCompleted", monitor, new object[2] { storeId, dataBase });
            bool? actualValue = Convert.ToBoolean(obj);
            Assert.AreEqual(expectedValue, actualValue);
        }

        [Test]
        public void ExpectedIsStoreCloseStarted()
        {
            bool expectedValue = true;
            string query = "SELECT isnull(Convert(Varchar(500),SDAT,106),'') as SDAT, isnull(Convert(Varchar(500),EDAT,106),'') as EDAT FROM NITLOG WHERE  DATE1 = '" + strDate + "'  And task = '005' order by TASK desc ";
            DataSet dsNitlogs = GetData(connectionString, query);
            if (dsNitlogs == null || dsNitlogs.Tables[0].Rows.Count == 0 || String.IsNullOrEmpty(Convert.ToString(dsNitlogs.Tables[0].Rows[0]["SDAT"])) || String.IsNullOrEmpty(Convert.ToString(dsNitlogs.Tables[0].Rows[0]["EDAT"])))
                expectedValue = false;
            object obj = TestUtilities.RunInstanceMethod(typeof(Monitor), "IsStoreCloseStarted", monitor, new object[2] { storeId, dataBase });
            bool? actualValue = Convert.ToBoolean(obj);
            Assert.AreEqual(expectedValue,actualValue);
        }

        [Test]
        public void ExpectedIsStoreCloseCompleted()
        {
            bool expectedValue = true;
            string query = "SELECT isnull(Convert(Varchar(500),SDAT,106),'') as SDAT, isnull(Convert(Varchar(500),EDAT,106),'') as EDAT FROM NITLOG WHERE  DATE1 = '" + strDate + "'  And task = '505' order by TASK desc ";
            DataSet dsNitlogs = GetData(connectionString, query);
            if (dsNitlogs == null || dsNitlogs.Tables[0].Rows.Count == 0 || String.IsNullOrEmpty(Convert.ToString(dsNitlogs.Tables[0].Rows[0]["SDAT"])) || String.IsNullOrEmpty(Convert.ToString(dsNitlogs.Tables[0].Rows[0]["EDAT"])))
                expectedValue = false;
            object obj = TestUtilities.RunInstanceMethod(typeof(Monitor), "IsStoreCloseCompleted", monitor, new object[2] { storeId, dataBase });
            bool? actualValue = Convert.ToBoolean(obj);
            Assert.AreEqual(expectedValue, actualValue);
        }

        [Test]
        public void ExpectedGetLastCompletedTask()
        {
            string query = "SELECT TOP 1 TASK,PROG FROM NITLOG WHERE  DATE1 = '" + strDate + "'  And SDAT is not null AND EDAT is not NULL order by TASK desc ";
            DataSet ds = GetData(connectionString, query);
            DataRow dr = ds.Tables[0].Rows[0];
            DataRow actualData = (DataRow)TestUtilities.RunInstanceMethod(typeof(Monitor), "GetLastCompletedTask", monitor, new object[2] { storeId, dataBase });
            IEqualityComparer<DataRow> comparer = DataRowComparer.Default;
            bool isMatch = comparer.Equals(dr, actualData);
            Assert.IsTrue(isMatch);
        }

        [Test]
        public void ExpectedGetData()
        {
            string query = "select 'SRV' + convert(varchar,Id) As Store from Stores where [Status] in ('L','P','T','W') AND IsClosed=0 AND Id<9000";
            DataSet actualData = (DataSet)TestUtilities.RunInstanceMethod(typeof(Monitor), "GetData", monitor, new object[2] { connectionString, query });
            DataSet expectedData = GetData(connectionString, query);
            actualData.Merge(expectedData);
            bool foundChanges = actualData.HasChanges();
            Assert.IsFalse(foundChanges); 
        }

        public DataSet GetData(string Connection, string query)
        {
            DataSet dsNitlogs = null;
            try
            {
                using (var con = new SqlConnection(Connection))
                {
                    if (con.State == ConnectionState.Closed)
                        con.Open();

                    using (var cmd = new SqlCommand(query, con))
                    {
                        using (var sda = new SqlDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (dsNitlogs = new DataSet())
                            {
                                sda.Fill(dsNitlogs, "Nitlog");
                                return dsNitlogs;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
